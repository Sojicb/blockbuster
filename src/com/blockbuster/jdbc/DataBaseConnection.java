package com.blockbuster.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.blockbuster.login.IncorrectInput;

public class DataBaseConnection {

	/**
	 * Method that is needed for connection to the database.
	 */
	private Connection connect() {
		String url = "jdbc:sqlite:blockbustergui.db";
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url);
		}catch(SQLException e){
			System.out.println(e.getMessage());
		}
		return conn;
	}

	public void insert(String username, String password, int admin) {
		String sql = "insert into user (username, password, isAdmin) values (?,?,?)";


		System.out.println(admin);
		try(Connection conn = this.connect();
				PreparedStatement ps = conn.prepareStatement(sql)){

			if(username.equals(password) || username.equals(null)) {
				return;
			}
			ps.setString(1, username);
			ps.setString(2, password);
			ps.setInt(3, admin);
			ps.executeUpdate();
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	public void printUser() {
		String sql = "SELECT * FROM user";
		try (Connection conn = this.connect();
				PreparedStatement pstmt  = conn.prepareStatement(sql)){
			//pstmt.setString(1, searchField.getText());

			ResultSet rs    = pstmt.executeQuery();
			while (rs.next()) {
				System.out.println(rs.getString("username") + "\t" + 
						rs.getString("password") + "\t" +
						rs.getString("isAdmin") + "\t" +
						rs.getString("userId"));
			}
			//searchField.setText(null);
		}catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}


	public String login(String username, String password) {
		String sql = "SELECT * FROM user where username = ? and password = ?";
		try (Connection conn = this.connect();
				PreparedStatement pstmt  = conn.prepareStatement(sql)){

			pstmt.setString(1,username);
			pstmt.setString(2,password);

			ResultSet rs  = pstmt.executeQuery();

			// loop through the result set
			return (rs.getString("username") + "" + rs.getString("password"));

			//searchField.setText(null);
		}catch (SQLException e) {
			System.out.println(e.getMessage());
			IncorrectInput incInp = new IncorrectInput();
			incInp.setVisible(true);
		}
		return null;
	}

	public void addMovie(String name, int price, int state) {
		String sql = "insert into movie (name, price, state) values (?,?,?)";


		try(Connection conn = this.connect();
				PreparedStatement ps = conn.prepareStatement(sql)){

			ps.setString(1, name);
			ps.setInt(2, price);
			ps.setInt(3, state);
			ps.executeUpdate();
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
	}


}
