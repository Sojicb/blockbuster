package com.blockbuster.movies;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.blockbuster.jdbc.DataBaseConnection;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class AddMovies extends JFrame {


	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField titleField;
	private JTextField stockField;
	private JTextField priceField;
	DataBaseConnection dbc = new DataBaseConnection();
	
	/**
	 * Launch the application.
	 */
	public void setupAdding() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddMovies frame = new AddMovies();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddMovies() {
		initAddingMovies();
	}
	
	public void initAddingMovies() {
		setTitle("Movies");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 313, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblMovieTitle = new JLabel("Movie Title:");
		lblMovieTitle.setBounds(7, 30, 71, 14);
		contentPane.add(lblMovieTitle);
		
		titleField = new JTextField();
		titleField.setBounds(77, 27, 196, 20);
		contentPane.add(titleField);
		titleField.setColumns(10);
		
		stockField = new JTextField();
		stockField.setBounds(77, 69, 196, 20);
		contentPane.add(stockField);
		stockField.setColumns(10);
		
		JLabel lblInStock = new JLabel("In Stock:");
		lblInStock.setBounds(7, 72, 60, 14);
		contentPane.add(lblInStock);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				dispose();
			}
		});
		btnCancel.setBounds(208, 238, 89, 23);
		contentPane.add(btnCancel);
		
		JButton addBt = new JButton("Add");
		addBt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dbc.addMovie(titleField.getText(), getInt(priceField.getText()), getInt(stockField.getText()));
				System.out.println(getInt(priceField.getText()));
				setVisible(false);
				dispose();
			}
		});
		addBt.setBounds(0, 238, 89, 23);
		contentPane.add(addBt);
		
		priceField = new JTextField();
		priceField.setBounds(77, 116, 196, 20);
		contentPane.add(priceField);
		priceField.setColumns(10);
		
		JLabel lblPrice = new JLabel("Price:");
		lblPrice.setBounds(7, 119, 46, 14);
		contentPane.add(lblPrice);
	}
	
	
	public int getInt(String number) {
		int result = Integer.parseInt(number);
		return result;
	}
}

