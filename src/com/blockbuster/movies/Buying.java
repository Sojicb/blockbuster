package com.blockbuster.movies;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Buying extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void initBuy(String title, boolean rented, String price) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Buying frame = new Buying(title, rented, price);
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param name 
	 * @param string 
	 * @param isBought 
	 * @param isRented 
	 */
	public Buying(String movieName, boolean rented, String price) {
		setTitle("Checkout");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 286, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnYes = new JButton("Okay");
		btnYes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
			}
		});
		btnYes.setBounds(84, 164, 89, 23);
		contentPane.add(btnYes);
		
		JLabel lblMovieTitle = new JLabel("Movie Title:");
		lblMovieTitle.setBounds(10, 59, 76, 14);
		contentPane.add(lblMovieTitle);
		
		JLabel lblStatus = new JLabel("Status:");
		lblStatus.setBounds(10, 84, 76, 14);
		contentPane.add(lblStatus);
		
		JLabel lblPrice = new JLabel("Price:");
		lblPrice.setBounds(10, 109, 76, 14);
		contentPane.add(lblPrice);
		
		JLabel title = new JLabel(movieName);
		title.setBounds(96, 59, 164, 14);
		contentPane.add(title);
		
		JLabel status = new JLabel();
		if(rented == true) {
			status.setText("For Rent");
		}else {
			status.setText("For Buying");
		}
		status.setBounds(96, 84, 164, 14);
		contentPane.add(status);
		
		JLabel moviePrice = new JLabel();
		moviePrice.setBounds(96, 109, 164, 14);
		moviePrice.setText(price);
		contentPane.add(moviePrice);
	}
	
	

}
