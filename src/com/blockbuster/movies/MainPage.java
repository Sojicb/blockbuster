package com.blockbuster.movies;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.blockbuster.jdbc.DataBaseConnection;
import com.blockbuster.movies.AddMovies;
import com.blockbuster.login.LogInForm;

import javax.swing.JRadioButton;
import javax.swing.JList;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MainPage extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtCena;

	DefaultListModel<String> dlm = new DefaultListModel<String>();
	DataBaseConnection dbc = new DataBaseConnection();
	LogInForm log = new LogInForm();
	public boolean isRented = false;
	public boolean isBought = false;
	public String movieName;

	/**
	 * Launch the application.
	 */
	public void initStore() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					String username = log.username.getText();
					MainPage frame = new MainPage(username);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainPage(String username) {
		initComponents(username);
	}

	private void initComponents(String username) {
		setTitle("Video Store");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 398, 352);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		txtCena = new JTextField();
		txtCena.setText("Price");
		txtCena.setEditable(false);
		txtCena.setBounds(73, 39, 66, 20);
		contentPane.add(txtCena);
		txtCena.setColumns(10);

		JLabel logged = new JLabel(username);
		logged.setBounds(73, 5, 81, 14);
		contentPane.add(logged);

		JRadioButton rbBuy = new JRadioButton("Buy");
		rbBuy.setBackground(new Color(204, 204, 255));
		rbBuy.setFont(new Font("Tahoma", Font.PLAIN, 14));
		rbBuy.setBounds(6, 82, 75, 23);
		contentPane.add(rbBuy);

		JRadioButton rbRent = new JRadioButton("Rent");
		rbRent.setBackground(new Color(204, 204, 255));
		rbRent.setFont(new Font("Tahoma", Font.PLAIN, 14));
		rbRent.setBounds(119, 82, 75, 23);
		contentPane.add(rbRent);

		rbBuy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(rbBuy.isSelected()) {
					isBought = true;
					isRented = false;
					rbRent.setSelected(false);
				}

			}
		});

		rbRent.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(rbRent.isSelected()) {
					isRented = true;
					isBought = false;
					rbBuy.setSelected(false);
				}

			}
		});

		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(true);
				dispose();
			}
		});
		btnClose.setBounds(286, 290, 89, 23);
		contentPane.add(btnClose);


		JList<String> movieDisplay = new JList<String>();
		movieDisplay.setBackground(new Color(255, 255, 255));
		movieDisplay.setBounds(200, 36, 175, 230); 
		contentPane.add(movieDisplay);
		movieDisplay.setModel(dlm);
		movieDisplay.setSelectedIndex(1);
		printMovies();
		movieDisplay.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				if (!arg0.getValueIsAdjusting()) {
					movieName = movieDisplay.getSelectedValue().toString();
					txtCena.setText(getPrice(movieName));
				}
			}
		});



		JLabel lblCenaFilma = new JLabel("Movie Price:");
		lblCenaFilma.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCenaFilma.setBounds(0, 36, 81, 23);
		contentPane.add(lblCenaFilma);

		JLabel lblMovieTitles = new JLabel("Movie Titles");
		lblMovieTitles.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblMovieTitles.setBounds(244, -1, 86, 29);
		contentPane.add(lblMovieTitles);

		JButton btnAddMovies = new JButton("Add Movies");
		btnAddMovies.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AddMovies add = new AddMovies();
				System.out.println("This" + isAdmin(username));
				
				if(isAdmin(username) != 0) {
					add.setLocationRelativeTo(null);
					add.setVisible(true);
				}else {
					notAdmin notAdmin = new notAdmin();
					notAdmin.setLocationRelativeTo(null);
					notAdmin.setVisible(true);
				}
			}
		});
		btnAddMovies.setBounds(45, 170, 109, 23);
		contentPane.add(btnAddMovies);

		JLabel lblLogedInAs = new JLabel("Loged in as:");
		lblLogedInAs.setBounds(0, 0, 75, 25);
		contentPane.add(lblLogedInAs);

		JButton btnPlati = new JButton("Pay");
		btnPlati.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(movieName != null && isBought != isRented) {
					Buying.initBuy(movieName, isRented, getPrice(movieName));
				}else {
					return;
						
				}
				
			}
		});
		btnPlati.setBounds(45, 136, 109, 23);
		contentPane.add(btnPlati);

		JLabel lblRsd = new JLabel("RSD");
		lblRsd.setBounds(149, 42, 56, 14);
		contentPane.add(lblRsd);
		
		JLabel label = new JLabel("");
		label.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				printMovies();
			}
		});
		label.setIcon(new ImageIcon("C:\\Users\\Dusan\\Desktop\\Dusan-Projektovanje\\project\\BlockBuster\\ref.jpg"));
		label.setBounds(200, 266, 20, 14);
		contentPane.add(label);

	}
	
	private Connection connect() {
		String url = "jdbc:sqlite:blockbustergui.db";
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url);
		}catch(SQLException e){
			System.out.println(e.getMessage());
		}
		return conn;
	}

	public void printMovies() {
		dlm.removeAllElements();
		String sql = "SELECT name, price, state FROM movie";
		try (Connection conn = this.connect();
				Statement stmt  = conn.createStatement();
				ResultSet rs    = stmt.executeQuery(sql)){

			while (rs.next()) {
				dlm.addElement((rs.getString("name")));
			}
		} catch (SQLException e1) {
			System.out.println(e1.getMessage());
		}
	}

	public String getPrice(String movie) {
		String sql = "SELECT price FROM movie where name = ?";
		try (Connection conn = this.connect();
				PreparedStatement pstmt  = conn.prepareStatement(sql)){

			pstmt.setString(1,movie);
			//
			ResultSet rs  = pstmt.executeQuery();

			while (rs.next()) {
				return rs.getString("price");
			}

		} catch (SQLException e1) {
			System.out.println(e1.getMessage());
		}
		return null;
	}

	public int isAdmin(String username){
		String sql = "SELECT isAdmin FROM user where username = ?";
		try (Connection conn = this.connect();
				PreparedStatement pstmt  = conn.prepareStatement(sql)){

			pstmt.setString(1,username);

			ResultSet rs  = pstmt.executeQuery();

			while (rs.next()) {
				
				return (rs.getInt("isAdmin"));
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println(username);
		return 0;
	}
}
