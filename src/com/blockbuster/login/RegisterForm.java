package com.blockbuster.login;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.blockbuster.jdbc.DataBaseConnection;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import java.awt.Color;

public class RegisterForm extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField usernameField;
	private JPasswordField passwordFieldOne;
	private JPasswordField passwordFieldTwo;
	private int admin;

	/**
	 * Launch the application.
	 */
	public void setupRegister() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegisterForm frame = new RegisterForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RegisterForm() {
		setTitle("Register Form");
		initRegister();
	}
	
	public void initRegister() {
DataBaseConnection connection = new DataBaseConnection();
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 325, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JCheckBox isAdmin = new JCheckBox("Is Admin");
		isAdmin.setBackground(new Color(204, 204, 255));
		isAdmin.setBounds(135, 132, 97, 23);
		contentPane.add(isAdmin);
		
		JLabel lblWelcomeToBlockbuster = new JLabel("Welcome to BlockBuster! Register Here:");
		lblWelcomeToBlockbuster.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblWelcomeToBlockbuster.setBounds(10, 11, 298, 20);
		contentPane.add(lblWelcomeToBlockbuster);
		
		JLabel lblName = new JLabel("Username");
		lblName.setBounds(10, 58, 72, 14);
		contentPane.add(lblName);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(10, 83, 72, 14);
		contentPane.add(lblPassword);
		
		JLabel lblRepeatPassword = new JLabel("Repeat Password");
		lblRepeatPassword.setBounds(10, 108, 116, 14);
		contentPane.add(lblRepeatPassword);
		
		
		
		usernameField = new JTextField();
		usernameField.setBounds(135, 55, 112, 20);
		contentPane.add(usernameField);
		usernameField.setColumns(10);
		
		JButton btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")

			public void actionPerformed(ActionEvent arg0) {
				if(isAdmin.isSelected()) {
					admin = 1;
				}else {
					admin = 0;
				}
				
				if (passwordFieldOne.getText().equals((passwordFieldTwo.getText()))) {
					connection.insert(usernameField.getText(), passwordFieldOne.getText(), admin);
				}else {
					return;
				}
				setVisible(false);
				dispose();
			}
			
		});
		btnRegister.setBounds(135, 171, 112, 20);
		contentPane.add(btnRegister);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			setVisible(false);
			dispose();
			}
		});
		btnCancel.setBounds(219, 238, 89, 23);
		contentPane.add(btnCancel);
		
		passwordFieldOne = new JPasswordField();
		passwordFieldOne.setBounds(135, 80, 112, 20);
		contentPane.add(passwordFieldOne);
		
		passwordFieldTwo = new JPasswordField();
		passwordFieldTwo.setBounds(136, 105, 111, 20);
		contentPane.add(passwordFieldTwo);
	}
}
