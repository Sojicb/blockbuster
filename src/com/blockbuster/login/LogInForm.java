package com.blockbuster.login;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.blockbuster.jdbc.DataBaseConnection;
import com.blockbuster.movies.MainPage;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import java.awt.Color;

public class LogInForm extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	public JTextField username;
	DataBaseConnection dbc = new DataBaseConnection();
	public JPasswordField password;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LogInForm frame = new LogInForm();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	public LogInForm() {
		initLogIn();
	}

	public void initLogIn() {
		setTitle("BlockBuster");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 257, 404);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		password = new JPasswordField();
		password.setBounds(121, 238, 86, 20);
		contentPane.add(password);

		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(9, 210, 70, 14);
		contentPane.add(lblUsername);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(10, 241, 69, 14);
		contentPane.add(lblPassword);

		JButton btnSignUp = new JButton("Sign up");
		btnSignUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				RegisterForm regform = new RegisterForm();
				regform.setLocationRelativeTo(null);
				regform.setVisible(true);
			}
		});
		btnSignUp.setBounds(118, 336, 89, 23);
		contentPane.add(btnSignUp);

		username = new JTextField();
		username.setBounds(121, 207, 86, 20);
		contentPane.add(username);
		username.setColumns(10);

		JLabel lblNotAMember = new JLabel("Not a Member?");
		lblNotAMember.setBounds(10, 340, 102, 14);
		contentPane.add(lblNotAMember);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Dusan\\Desktop\\Dusan-Projektovanje\\project\\BlockBuster\\logo.png"));
		lblNewLabel.setBounds(9, 11, 222, 150);
		contentPane.add(lblNewLabel);

		JButton btnLogIn = new JButton("Sign in");
		btnLogIn.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				if(dbc.login(username.getText(), password.getText()).equals(username.getText() + "" + password.getText())) {
					MainPage mp = new MainPage(username.getText());
					mp.setLocationRelativeTo(null);
					mp.setVisible(true);
					setVisible(false);
					dispose();
				}else {
					IncorrectInput incInp = new IncorrectInput();
					incInp.setLocationRelativeTo(null);
					incInp.setVisible(true);
				}
			}
		});
		btnLogIn.setBounds(118, 285, 89, 23);
		contentPane.add(btnLogIn);
	}
}
